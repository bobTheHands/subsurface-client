#include <assert.h>
#include <cairo.h>

#include "base.h"

static cairo_surface_t *image;
static struct pool_buffer pool[N_POOL_BUFFERS];

static union {
    struct {
        struct subsurface tl, tr, bl, br;
    };
    struct subsurface raw[4];
} subs;

static void handle_configure() {
    int w2 = window.surface.width / 2, h2 = window.surface.height / 2;
    wl_subsurface_set_position(subs.tl.wl_subsurface, 0, 0);
    wl_subsurface_set_position(subs.tr.wl_subsurface, w2, 0);
    wl_subsurface_set_position(subs.bl.wl_subsurface, 0, h2);
    wl_subsurface_set_position(subs.br.wl_subsurface, w2, h2);
    for (int i = 0; i < 4; i++) {
        struct subsurface *subsurface = &subs.raw[i];
        struct surface *surface = &subsurface->surface;
        surface->width = w2;
        surface->height = h2;
        wp_viewport_set_destination(surface->viewport, w2, h2);
        surface_render(&subs.raw[i].surface);
    }
}

static void render(
        struct surface *surface, struct pool_buffer *buffer, cairo_t *cairo) {
    struct pool_buffer *image_buffer = get_next_buffer(
            shm, pool, window.surface.width, window.surface.height);
    if (!image_buffer) {
        fprintf(stderr, "failed to obtain image buffer\n");
        return;
    }

    if (surface == &window.surface) {
        cairo_set_operator(image_buffer->cairo, CAIRO_OPERATOR_SOURCE);
        cairo_matrix_t matrix;
        cairo_matrix_init(&matrix, -1, 0, 0, -1, image_buffer->width,
                image_buffer->height);
        cairo_set_matrix(image_buffer->cairo, &matrix);
        cairo_set_source_surface(image_buffer->cairo, image, 0, 0);
        cairo_paint(image_buffer->cairo);
        image_buffer->busy = true;

        float *color = surface->color;
        cairo_set_operator(cairo, CAIRO_OPERATOR_SOURCE);
        cairo_set_source_rgba(cairo, color[0], color[1], color[2], 1.0f);
        cairo_paint(cairo);
        wl_surface_attach(surface->wl_surface, buffer->buffer, 0, 0);
        buffer->busy = true;
        return;
    }

    if (surface->viewport) {
        int x = 0, y = 0;
        if (surface == &subs.tr.surface || surface == &subs.br.surface) {
            x = window.surface.width / 2;
        }
        if (surface == &subs.bl.surface || surface == &subs.br.surface) {
            y = window.surface.height / 2;
        }
        wp_viewport_set_source(surface->viewport, wl_fixed_from_int(x),
                wl_fixed_from_int(y), wl_fixed_from_int(surface->width),
                wl_fixed_from_int(surface->height));
    }
    wl_surface_attach(surface->wl_surface, image_buffer->buffer, 0, 0);
}

int main() {
    image = cairo_image_surface_create_from_png("image.png");
    assert(image);

    user_configure_handler = handle_configure;
    user_renderer = render;
    base_init();

    if (!viewporter) {
        fprintf(stderr, "Couldn't get viewporter\n");
        exit(1);
    }

    for (int i = 0; i < 4; i++) {
        struct subsurface *subsurface = &subs.raw[i];
        struct surface *surface = &subsurface->surface;
        subsurface_init(subsurface, &window.surface, 0, 0,
                (float[]){0.0f, 1.0f, 0.0f, 1.0f});
        surface->viewport
                = wp_viewporter_get_viewport(viewporter, surface->wl_surface);
        wl_surface_set_buffer_transform(
                surface->wl_surface, WL_OUTPUT_TRANSFORM_180);
    }

    wl_surface_commit(window.surface.wl_surface);

    while (wl_display_dispatch(display) != -1) {
        /* Wait */
    }
    return 0;
}
