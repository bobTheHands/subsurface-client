# subsurface-client

A bunch of wacky and uncharacteristic Wayland clients, doing subsurface things that a normal client wouldn't do.

## Building

```sh
meson build/
meson compile -C build/
```

The resulting clients will be located in `build/client`.

## Clients

- `advance` ­— checks whether the surface state is applied at the right time.
- `frame-callback` — checks whether a subsurface received a frame event at the right time.
- `order-above`, `order-below` — check whether the compositor handles noop `place_above`, `place_below` requests properly.
- `position-commit` — checks whether subsurface-specific state is applied at the right time.
- `grandchild-sync` — a client for a thought experiment described [here](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/merge_requests/26#note_950041).
- `split-flip` — checks whether the compositor applies coordinate transformations correctly. (Requires 'image.png' to be present in `$PWD`.)
- `exhauster` — checks whether the compositor handles cached state correctly and doesn't hold on client resources for more than required.

## Acknowledgements

`common/pool-buffer.c`, `common/pool-buffer.h` and majority of code in `common/base.c` are taken from [emersion/wleird](https://github.com/emersion/wleird).
