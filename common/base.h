#ifndef _BASE_H
#define _BASE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wayland-client-protocol.h>

#include "pool-buffer.h"
#include "viewporter-protocol.h"
#include "xdg-shell-client-protocol.h"

extern struct wl_display *display;
extern struct wl_shm *shm;
extern struct wl_compositor *compositor;
extern struct wl_subcompositor *subcompositor;
extern struct wp_viewporter *viewporter;
extern struct xdg_wm_base *wm_base;
extern struct wl_data_device_manager *data_device_manager;
extern struct wl_seat *seat;
extern struct wl_keyboard *keyboard;

struct surface;

extern void (*user_key_handler)(uint32_t key, uint32_t state);
extern void (*user_configure_handler)();
extern void (*user_renderer)(
        struct surface *surface, struct pool_buffer *buffer, cairo_t *cairo);

struct surface {
    struct wl_surface *wl_surface;
    int width, height;
    struct pool_buffer buffers[N_POOL_BUFFERS];

    struct wp_viewport *viewport;

    float color[3];
};

struct subsurface {
    struct surface surface;
    struct wl_subsurface *wl_subsurface;
};

struct window {
    struct surface surface;
    struct xdg_surface *xdg_surface;
    struct xdg_toplevel *xdg_toplevel;
};

extern struct window window;

void surface_init(struct surface *surface, int width, int height, float *color);
void surface_render(struct surface *surface);

void subsurface_init(struct subsurface *subsurface, struct surface *parent,
        int x, int y, float *color);

void base_init();

#endif
